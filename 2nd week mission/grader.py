# Q3. 학점 변환기
def grader(student_name, score):
  if score >= 95 : 
    grade = 'A+'
  elif score >= 90 :
    grade = 'A'
  elif score >= 85 :
    grade = 'B+'
  elif score >= 80 :
    grade = 'B'
  elif score >= 75 :
    grade = 'C+'
  elif score >= 70 :
    grade = 'C'
  elif score >= 65 :
    grade = 'D+'
  elif score >= 60 :
    grade = 'D'
  else : 
    grade = 'F'
  return grade
​
def output(student_name, score,grade):
  print('학생이름 : ',student_name)
  print('점수 : ',score,'점')
  print('학점 : ',grade)
​
student_name = input('학생의 이름을 입력해주세요 : ')
score = int(input('학생의 점수를 입력해주세요 : '))
grade = grader(student_name, score)
output(student_name, score,grade)
# try except로 입력 범위 제한 필요
