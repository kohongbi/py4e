def yearly_payment(monthly_payment):
  annual_salary = monthly_payment*12
  return annual_salary
​
def tax_annual_salary(annual_salary):
  if annual_salary <= 1200 : 
    tax_included_annual_salary = annual_salary*0.94
  elif annual_salary <=4600 : 
    tax_included_annual_salary = annual_salary*0.85
  elif annual_salary <= 8800:
    tax_included_annual_salary = annual_salary*0.76
  elif annual_salary <= 15000:
    tax_included_annual_salary = annual_salary*0.65
  elif annual_salary <= 30000:
    tax_included_annual_salary = annual_salary*0.62
  elif annual_salary <= 50000:
    tax_included_annual_salary = annual_salary*0.60
  else :
    tax_included_annual_salary = annual_salary*0.58
  return tax_included_annual_salary
​
def output(annual_salary,tax_included_annual_salary):
  print('세전 연봉 : ',annual_salary,'만원')
  print('세후 연봉 : ',int(tax_included_annual_salary),'만원')
​
​
monthly_payment = int(input('당신의 월급을 입력하세요 (ex]3,200,000인 경우 320) : '))
annual_salary = yearly_payment(monthly_payment)
tax_included_annual_salary = tax_annual_salary(annual_salary)
output(annual_salary,tax_included_annual_salary)

#try except가 필요할지 고민
